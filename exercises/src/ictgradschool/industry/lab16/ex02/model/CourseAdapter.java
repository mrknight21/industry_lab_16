package ictgradschool.industry.lab16.ex02.model;


import javax.swing.table.AbstractTableModel;

public class CourseAdapter extends AbstractTableModel implements CourseListener{


	private Course course;

	public CourseAdapter(Course course){
		this.course = course;
		this.course.addCourseListener(this);
	}


	@Override
	public void courseHasChanged(Course course) {
		fireTableDataChanged();
	}

	@Override
	public int getRowCount() {
		return course.size();
	}

	@Override
	public int getColumnCount() {
		return 7;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		StudentResult student = course.getResultAt(rowIndex);

		if (columnIndex >=0 && columnIndex <7) {
			switch (columnIndex) {
				case 0:
					return student._studentID;
				case 1:
					return student._studentSurname;
				case 2:
					return student._studentForename;
				case 3:
					return student.getAssessmentElement(StudentResult.AssessmentElement.Exam);
				case 4:
					return student.getAssessmentElement(StudentResult.AssessmentElement.Test);
				case 5:
					return student.getAssessmentElement(StudentResult.AssessmentElement.Assignment);
				case 6:
					return student.getAssessmentElement(StudentResult.AssessmentElement.Overall);
			}
		}
		return null;
	}



	/**********************************************************************
	 * YOUR CODE HERE
	 */
}