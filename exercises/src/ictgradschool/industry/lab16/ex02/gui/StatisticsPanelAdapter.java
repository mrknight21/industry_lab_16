package ictgradschool.industry.lab16.ex02.gui;

import ictgradschool.industry.lab16.ex02.model.Course;
import ictgradschool.industry.lab16.ex02.model.CourseListener;

public class StatisticsPanelAdapter implements CourseListener{
	
	/**********************************************************************
	 * YOUR CODE HERE
	 */
	private StatisticsPanel panel;


	public StatisticsPanelAdapter(StatisticsPanel panel){
		this.panel =panel;
	}

	@Override
	public void courseHasChanged(Course course) {
		panel.repaint();
	}

}
