package ictgradschool.industry.lab16.ex02.gui;

import ictgradschool.industry.lab16.ex02.model.Course;
import ictgradschool.industry.lab16.ex02.model.CourseListener;

public class DistributionPanelAdapter implements CourseListener{
	/**
	 * Creates a DistributionPanel object.
	 *
	 * @param course
	 */
private DistributionPanel panel;


public DistributionPanelAdapter(DistributionPanel panel){
	this.panel =panel;
}

	/**
	 * Creates a DistributionPanel object.
	 *
	 * @param course
	 */


	@Override
	public void courseHasChanged(Course course) {
		panel.repaint();
	}


	/**********************************************************************
	 * YOUR CODE HERE
	 */
}
